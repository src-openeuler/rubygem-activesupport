%global gem_name activesupport
Name:                rubygem-%{gem_name}
Epoch:               1
Version:             7.0.7
Release:             2
Summary:             A support libraries and Ruby core extensions extracted from the Rails framework
License:             MIT
URL:                 http://rubyonrails.org
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# The activesupport gem doesn't ship with the test suite.
# You may check it out like so
# git clone http://github.com/rails/rails.git
# cd rails/activesupport && git archive -v -o activesupport-7.0.7-tests.txz v7.0.7 test/
Source1:             %{gem_name}-%{version}-tests.txz
# The tools are needed for the test suite, are however unpackaged in gem file.
# You may get them like so
# git clone http://github.com/rails/rails.git --no-checkout
# cd rails && git archive -v -o rails-7.0.7-tools.txz v7.0.7 tools/
Source2:             rails-%{version}-tools.txz
# Fixes for Minitest 5.16+
# https://github.com/rails/rails/pull/45380
Patch1: rubygem-activesupport-7.0.2.3-Remove-the-multi-call-form-of-assert_called_with.patch
Patch2: rubygem-activesupport-7.0.2.3-Remove-the-multi-call-form-of-assert_called_with-test.patch
# https://github.com/rails/rails/pull/45370
Patch3: rubygem-activesupport-7.0.2.3-Fix-tests-for-minitest-5.16.patch
Patch4: CVE-2023-38037.patch
Patch5: CVE-2023-38037-test.patch

Requires:            rubygem(bigdecimal) rubygem(json)
BuildRequires:       ruby(release) rubygems-devel ruby >= 2.2.2 rubygem(bigdecimal) rubygem(builder)
BuildRequires:       rubygem(concurrent-ruby) rubygem(connection_pool) rubygem(dalli)
BuildRequires:       (rubygem(i18n) >= 0.7 with rubygem(i18n) < 2) rubygem(minitest) >= 5.0.0
BuildRequires:       rubygem(rack) rubygem(tzinfo) >= 2.0 rubygem(listen) rubygem(redis) memcached rubygem(rexml)
BuildArch:           noarch
%description
A toolkit of support libraries and Ruby core extensions extracted from the
Rails framework. Rich support for multibyte strings, internationalization,
time zones, and testing.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{epoch}:%{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1 -b2
%patch1 -p2
%patch3 -p2
%patch4 -p2

pushd %{_builddir}
%patch2 -p2
%patch5 -p2
popd

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/tools ..
mv %{_builddir}/test .
# redis_cache_store_test: failed to require "redis/connection/hiredis"
for f in \
  test/evented_file_update_checker_test.rb \
  test/cache/stores/redis_cache_store_test.rb \
  test/cache/stores/mem_cache_store_test.rb
do
  mv $f{,.disable}
done
sed -i '/def test_iso8601_output_and_reparsing$/,/^  end$/ s/^/#/' test/core_ext/duration_test.rb
sed -i '/assert_nil mapped\[:b\]/ s/^/#/' test/core_ext/hash/transform_values_test.rb
sed -i '/require .bundler./ s/^/#/' test/abstract_unit.rb
memcached &
mPID=$!
sleep 1
ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
kill -15 $mPID
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc

%changelog
* Mon Sep 11 2023 wangkai <13474090681@163.com> - 1:7.0.7-2
- Fix CVE-2023-38037

* Fri Aug 18 2023 wangkai <wang_kai001@hoperun.com> - 1:7.0.7-1
- Upgrade to version 7.0.7

* Thu Aug 17 2023 liqiuyu <liqiuyu@kylinos.cn> - 1:7.0.6-1
- Upgrade to version 7.0.6

* Mon Aug 14 2023 caiyuxin <caiyuxin@kylinos.cn> - 1:7.0.4-3
- Backport upstream fix for test failure with ruby3.2 wrt class_serial removal

* Tue Feb 21 2023 wushaozheng <wushaozheng@ncti-gba.cn> - 1:7.0.4-2
- fix CVE-2023-22796 

* Thu Jan 19 2023 yanxiaobing <yanxiaobing@huawei.com> - 1:7.0.4-1
- Upgrade to version 7.0.4

* Tue Jul 05 2022 liyanan <liyanan32@h-partners.com> - 6.1.4.1-2
- Add support dalli 3.2.2

* Wed Mar 02 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 6.1.4.1-1
- update to 6.1.4.1

* Mon Feb  8 2021 sunguoshuai <sunguoshuai@huawei.com> - 5.2.4.4-1
- Upgrade to 5.2.4.4

* Sat Sep 5 2020 liyanan <liyanan32@huawei.com> - 5.2.3-2
- fix build fail

* Wed Aug 12 2020 chengzihan <chengzihan2@huawei.com> - 5.2.3-1
- Package init
